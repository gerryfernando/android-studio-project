package com.example.impal;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.impal.cinemakuy.CinemakuyAPI;
import com.example.impal.cinemakuy.CinemakuyAPIStatic;
import com.example.impal.cinemakuy.MovieNowPlayingData;
import com.example.impal.cinemakuy.MovieSchedule;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity {

    MovieNowPlayingData.DataMovie dataFilm;
    CinemakuyAPI cinemakuyAPI = CinemakuyAPIStatic.create();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Gson gson = new Gson();
        dataFilm = gson.fromJson(getIntent().getStringExtra("dataFilm"), MovieNowPlayingData.DataMovie.class);
        dataFilm.bitmap = (Bitmap) getIntent().getParcelableExtra("cover");

        ShowDataFilm();
        getCinema();
    }
    void ShowDataFilm(){
        Toast.makeText(this, "Name : "+dataFilm.name+" bitmap : "+(dataFilm.bitmap==null), Toast.LENGTH_SHORT).show();
    }
    void getCinema(){
        SharedPreferences prefs = this.getSharedPreferences(
                "movie", Context.MODE_PRIVATE);
        Call<MovieSchedule> movieScheduleCall = cinemakuyAPI.getMovieSchedule(dataFilm.event_id , prefs.getString("city" , "Jakarta"));

        Toast.makeText(this, "eventID:"+dataFilm.event_id+"city:"+prefs.getString("city" , "Jakarta"), Toast.LENGTH_SHORT).show();
        movieScheduleCall.enqueue(new Callback<MovieSchedule>() {
            @Override
            public void onResponse(Call<MovieSchedule> call, Response<MovieSchedule> response) {
                MovieSchedule movieSchedule = response.body();
                String date = movieSchedule.data.showdates.get(0).date;
                String ciname = movieSchedule.data.cinemas.get(0).provider_name;
                Toast.makeText(DetailActivity.this,"cinema :"+ciname+"date :" +date, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MovieSchedule> call, Throwable t) {
                Toast.makeText(DetailActivity.this, "no internet connection", Toast.LENGTH_SHORT).show();
            }
        });

    }
    void getCinema(String date){
        SharedPreferences prefs = this.getSharedPreferences(
                "movie", Context.MODE_PRIVATE);
        cinemakuyAPI.getMovieSchedule(dataFilm.event_id , prefs.getString("city" , "Jakarta") , date);
        Toast.makeText(this, "eventID:"+dataFilm.event_id+"city:"+prefs.getString("city" , "Jakarta"), Toast.LENGTH_SHORT).show();
    }

}
