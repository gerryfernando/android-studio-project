package com.example.impal.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.impal.HomepageActivity;
import com.example.impal.R;
import com.example.impal.cinemakuy.MovieNowPlayingData;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class AdapterMovieHomepage extends RecyclerView.Adapter<AdapterMovieHomepage.MovieViewHolder> {

    MovieNowPlayingData dataMovies;
    Activity activity;

    public AdapterMovieHomepage(MovieNowPlayingData dataMovies , Activity activity){
        this.dataMovies = dataMovies;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.movie_card, viewGroup, false);
        return new MovieViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder movieViewHolder, int i){

        movieViewHolder.txtTittle.setText(dataMovies.data.get(i).name);
        movieViewHolder.update(i);
        movieViewHolder.directorName.setText(dataMovies.data.get(i).director);
        movieViewHolder.update(i);


    }

    @Override
    public int getItemCount() {
        return dataMovies==null ? 0: dataMovies.data.size();
    }

    public void clear() {
        int size = getItemCount();
        this.dataMovies=null;
        notifyItemRangeRemoved(0,size);
    }
    public void generate(MovieNowPlayingData movieNowPlayingData) {
        this.dataMovies = movieNowPlayingData;
        notifyItemRangeChanged(0 , getItemCount());
        notifyDataSetChanged();
    }




    public class MovieViewHolder extends RecyclerView.ViewHolder{
        public int currPos;
        private TextView txtTittle;
        private ImageView imgCover;
        private TextView directorName;

        public void update(int i){
            currPos = i;
            if(dataMovies.data.get(i).bitmap == null){
                Bitmap bm = BitmapFactory.decodeFile("@mipmap/ic_launcher_round");
                imgCover.setImageBitmap(bm);
                //return;
            }else {
                imgCover.setImageBitmap(dataMovies.data.get(i).bitmap);
            }
        }

        public MovieViewHolder(View itemView) {
            super(itemView);
            txtTittle = (TextView) itemView.findViewById(R.id.tittle_homepage);
            imgCover = (ImageView) itemView.findViewById(R.id.cover_homepage);

            //movieViewHolder.
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((HomepageActivity) activity).ToDetail(currPos);
                }
            });

            directorName = (TextView) itemView.findViewById(R.id.director_name);

        }
    }


}
