package com.example.impal;

import android.content.Context;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.impal.Loading.ViewDialog;
import com.example.impal.cinemakuy.CinemakuyAPI;
import com.example.impal.cinemakuy.CinemakuyAPIStatic;
import com.example.impal.cinemakuy.GetAccResponse;
import com.example.impal.cinemakuy.LoginResponse;
import com.example.impal.cinemakuy.RequestGetAcc;
import com.example.impal.cinemakuy.RequestLogin;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    ViewDialog viewDialog;
    CinemakuyAPI cinemakuyAPI = CinemakuyAPIStatic.create();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        Toast.makeText(LoginActivity.this, "Welcome to cinemakuy" , Toast.LENGTH_SHORT).show();

        // create loading dialog
        viewDialog = new ViewDialog(this);
        LoginCheking();
    }

    public void LoginCheking(){
        viewDialog.showDialog();
        final String username = PreferencesAccount.PreferencesGet(LoginActivity.this).getString("USERNAME","");
        String token = PreferencesAccount.PreferencesGet(LoginActivity.this).getString("TOKEN","");

        if(token!=""){
            Call<GetAccResponse> getAccResponseCall = cinemakuyAPI.getAcc(new RequestGetAcc(username,token));

            getAccResponseCall.enqueue(new Callback<GetAccResponse>() {
                @Override
                public void onResponse(Call<GetAccResponse> call, Response<GetAccResponse> response) {

                    GetAccResponse getAccResponse = response.body();
                    if(getAccResponse.isSuccess!=null && getAccResponse.isSuccess){

                        Intent myIntent = new Intent(LoginActivity.this, /*Change this activity to home*/HomepageActivity.class);
                        LoginActivity.this.startActivity(myIntent);
                    }else{
                        Toast.makeText(LoginActivity.this, username , Toast.LENGTH_SHORT).show();
                    }
                    viewDialog.hideDialog();

                }

                @Override
                public void onFailure(Call<GetAccResponse> call, Throwable t) {
                    viewDialog.hideDialog();
                }
            });
        }else{
            viewDialog.hideDialog();

        }
    }

    public void LoginProcess(final String username,String pass){

        Log.d("retrofit" , "username :"+username+" pass :"+pass);
        // Showing Dialog Loading
        viewDialog.showDialog();
        //Create cinemakuy API
        CinemakuyAPI cinemakuyAPI = CinemakuyAPIStatic.create();
        // Call account/login
        Call<LoginResponse> obj = cinemakuyAPI.getLogin( new RequestLogin( username , pass ) );
        //Waiting for API response
        Log.d("retrofit" , "login start");
        obj.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                // hide Loading
                viewDialog.hideDialog();

                // Procesing Loading data
                LoginResponse result = response.body();

                Toast.makeText(LoginActivity.this, "Result : "+result.respon, Toast.LENGTH_SHORT).show();
                Log.d("retrofit" , "get login finish");

                // is logged in successfully?
                if(result.isSuccess) {
                    // Login success

                    // Change Layout to home or etc
                    PreferencesAccount.PreferencesAdd("TOKEN", result.token, LoginActivity.this);
                    PreferencesAccount.PreferencesAdd("USERNAME", username, LoginActivity.this);
                    //String token = PreferencesAccount.PreferencesGet(this).getString("TOKEN" , null);
                    Intent myIntent = new Intent(LoginActivity.this, /*Change this activity to home*/HomepageActivity.class);
                    LoginActivity.this.startActivity(myIntent);
                }else{
                    // Login Failed

                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                // Login failed because no internet connection
                viewDialog.hideDialog();
                Log.d("retrofit" , "get login error");
                Toast.makeText(LoginActivity.this, "Error : No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });



    }

    public void Login(View view) {
        // Define edit text
        EditText userET = (EditText)findViewById(R.id.user_login);
        EditText passET = (EditText)findViewById(R.id.pass_login);
        //TextView tview = (TextView)findViewById(R.id.textview1);
        String username = userET.getText().toString();
        String password = passET.getText().toString();

        //process value
        LoginProcess(username , password);
    }

    public void Register(View view) {
        // move to register activity
        Intent myIntent = new Intent(LoginActivity.this, RegisterActivity.class);
        LoginActivity.this.startActivity(myIntent);
    }
}
