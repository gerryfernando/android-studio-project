package com.example.impal.cinemakuy;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CinemakuyAPIStatic {

    public static CinemakuyAPI create() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://cinemakuy.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();



        return  retrofit.create(CinemakuyAPI.class);
    }

}
