package com.example.impal.cinemakuy;

public class RequestUpdate {
    private String name;
    private String username;
    private String pass;
    private String email;
    private String phone;

    public RequestUpdate(String name, String username, String pass, String email, String phone) {
        this.name = name;
        this.username = username;
        this.pass = pass;
        this.email = email;
        this.phone = phone;
    }
}
