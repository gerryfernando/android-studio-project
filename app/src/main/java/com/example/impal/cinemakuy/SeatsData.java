package com.example.impal.cinemakuy;

import java.util.ArrayList;

public class SeatsData {
    public class SeatsSchedule {
        public String provider;
        public int max_booked_seats;
        public String audi;
        public int width;
        public int height;
        public String schedule_class;
        public int schedule_id;

        public class SeatLayout {
            public String section_id;
            public String section_name;
            public int width;
            public int height;
            public int price;

            public class Seats {
                public String code;
                public int row;
                public int column;
                public String flag;
                public int price;
                public int status;
                public int ticket_id;
                public String coordinate;
                public boolean seat_in_group;
            }

            public ArrayList<Seats> seats = new ArrayList<>();
        }

        public ArrayList<SeatLayout> seat_layout = new ArrayList<>();
        public String provider_image;
    }

    public SeatsSchedule data = new SeatsSchedule();
    public int status;
    public String message;
}
