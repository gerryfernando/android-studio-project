package com.example.impal.cinemakuy;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/// More Info https://gitlab.com/impal-group-1/project-tibiko
/// base url : https://cinemakuy.herokuapp.com/
public interface CinemakuyAPI {

    /// Movies : https://gitlab.com/impal-group-1/project-tibiko#list-of-indonesian-cities-get
    @GET("movies/cities")
    Call<ArrayList<String>> getCity();

    /// Movies : https://gitlab.com/impal-group-1/project-tibiko#now-playing-movies-get
    @GET("movies/now_playing")
    Call<MovieNowPlayingData> getNowPlaying(@Query("city") String city);

    /// Movies : https://gitlab.com/impal-group-1/project-tibiko#schedule-movies-on-cinema-get
    @GET("movies/schedule")
    Call<MovieSchedule> getMovieSchedule(@Query("event_id") int idMovie , @Query("city") String city , @Query("date") String date);

    /// Movies : https://gitlab.com/impal-group-1/project-tibiko#schedule-movies-on-cinema-get
    @GET("movies/schedule")
    Call<MovieSchedule> getMovieSchedule(@Query("event_id") int idMovie , @Query("city") String city);

    /// Movies : https://gitlab.com/impal-group-1/project-tibiko#seat-on-cinema-get
    @GET("movies/schedule/{schedule_id}/seats")
    Call<SeatsData> getMovieScheduleSeats(@Path("schedule_id") String schedule_id);

    /// Movies : https://gitlab.com/impal-group-1/project-tibiko#ordering-seats-checkout-seats-post
    @POST("movies/order")
    Call<SeatOrderResponse> getMovieOrderSeats(@Body SeatOrderRequest SOR);


    /// Movies : https://gitlab.com/impal-group-1/project-tibiko#purchase-seats-buy-seats-post
    @POST("movies/pay")
    Call<SeatPayResponse> getMoviePaySeats(
            @Field("order_id") int order_id,
            @Field("payment_provider_type")int payment_provider_type,
            @Field("username") String username,
            @Field("email") String email,
            @Field("total_price") String total_price
    );

    /// Movies : https://gitlab.com/impal-group-1/project-tibiko#check-purchase-status-get
    @GET("movies/pay/{id_order}")
    Call<CheckPurchaseStatus> getMoviePaySeatsStatus(@Path("id_order") int id_order);


    /// Payment : https://gitlab.com/impal-group-1/project-tibiko#make-a-session-payment-via-virtual-account-post
    @POST("payment/va")
    Call<CreditPaymentResponse> fillCreditPaymentVA(
            @Field("price") int price,
            @Field("channel")String channel,
            @Field("username") String username,
            @Field("email") String email,
            @Field("phone") String phone
    );

    /// Payment : https://gitlab.com/impal-group-1/project-tibiko#make-a-session-payment-via-virtual-account-post
    @POST("payment/cstore")
    Call<CreditPaymentResponse> fillCreditPaymentCStore(
            @Field("price") int price,
            @Field("channel")String channel,
            @Field("username") String username,
            @Field("email") String email,
            @Field("phone") String phone
    );

    /// Account : https://gitlab.com/impal-group-1/project-tibiko#login-user-account-get
    @POST("account/login")
    Call<LoginResponse> getLogin(@Body RequestLogin requestLogin);


    /// Account : https://gitlab.com/impal-group-1/project-tibiko#registration-user-account-post
    @POST("account/registration")
    Call<LoginResponse> register(
            @Body RequestRegister requestRegister
    );


    ///Account : https://https://gitlab.com/impal-group-1/project-tibiko#update-user-account-post
    @POST("account/update")
    Call<UpdateResponse> getUpdate(
            @Body RequestUpdate requestUpdate
    );


    ///Account : https://gitlab.com/impal-group-1/project-tibiko#check-user-account-get
    @POST("account/get")
    Call<GetAccResponse> getAcc(
            @Body RequestGetAcc requestGetAcc
    );




}
