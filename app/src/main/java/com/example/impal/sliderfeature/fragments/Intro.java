package com.example.impal.sliderfeature.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.impal.R;

public class Intro extends Fragment {

    private String text;
    public void setTittle(String text){

        this.text=text;
    }

    public Intro(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_intro, container, false);


        ((TextView)rootView.findViewById(R.id.tittle_intro)).setText(text);
        return rootView;

        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_intro, container, false);
    }

}
