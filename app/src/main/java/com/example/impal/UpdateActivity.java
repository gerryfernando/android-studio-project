package com.example.impal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


import com.example.impal.cinemakuy.CinemakuyAPI;
import com.example.impal.cinemakuy.GetAccResponse;
import com.example.impal.cinemakuy.RequestGetAcc;
import com.example.impal.cinemakuy.RequestUpdate;
import com.example.impal.cinemakuy.UpdateResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UpdateActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_activity);

    }


    public void UpdateProcess(String name, String username, String pass, String email,  String phone){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://cinemakuy.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        CinemakuyAPI cinemakuyAPI = retrofit.create(CinemakuyAPI.class);
        String token = PreferencesAccount.PreferencesGet(UpdateActivity.this).getString("TOKEN" , null);
        Call<GetAccResponse> objcek = cinemakuyAPI.getAcc(new RequestGetAcc(username,token));
        final RequestUpdate requestUpdate = new RequestUpdate(name, username, pass, email,phone);
        Log.d("retrofit", "get acc start");
        objcek.enqueue(new Callback<GetAccResponse>() {
            @Override
            public void onResponse(Call<GetAccResponse> call, Response<GetAccResponse> response) {
                GetAccResponse res = response.body();
                if (res.isSuccess) {
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl("https://cinemakuy.herokuapp.com/")
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    CinemakuyAPI cinemakuyAPI1 = retrofit.create(CinemakuyAPI.class);
                    Call<UpdateResponse> obj = cinemakuyAPI1.getUpdate(requestUpdate);
                    Log.d("retrofit", "update start");
                    obj.enqueue(new Callback<UpdateResponse>() {
                        @Override
                        public void onResponse(Call<UpdateResponse> call, Response<UpdateResponse> response) {
                            UpdateResponse result = response.body();
                            Log.d("retrofit", "get login finish");
                            if (result.isSuccess) {
                                // Change Layout to home or etc
                                Intent myIntent = new Intent(UpdateActivity.this, /*Change this activity to home*/TestAPI.class);
                                UpdateActivity.this.startActivity(myIntent);

                            }
                        }

                        @Override
                        public void onFailure(Call<UpdateResponse> call, Throwable t) {
                            Log.d("retrofit", "update account error");
                            //Toast.makeText(UpdateActivity.this, "Something Error : "+t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            }

            @Override
            public void onFailure(Call<GetAccResponse> call, Throwable t) {
                Log.d("retrofit" , "get account error");
                //Toast.makeText(UpdateActivity.this, "Something Error : "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void Update(View view) {
        EditText passET = (EditText)findViewById(R.id.pass_login);
        EditText nameET = (EditText)findViewById(R.id.user_login);
        EditText emailET = (EditText)findViewById(R.id.pass_login);
        EditText phoneET = (EditText)findViewById(R.id.user_login);

        //TextView tview = (TextView)findViewById(R.id.textview1);
        String username = PreferencesAccount.PreferencesGet(UpdateActivity.this).getString("USERNAME",null);
        String password = passET.getText().toString();
        String name = nameET.getText().toString();
        String email = emailET.getText().toString();
        String phone = phoneET.getText().toString();
        UpdateProcess(name,  username,  password, email, phone);
    }

}
